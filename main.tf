resource "google_compute_instance" "default" {
  name = "${var.application_name}-${terraform.workspace}"
  machine_type = var.machine_type
  zone         = "europe-west1-b"
  project = var.gcp_project

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-9"
    }
  }

  network_interface {
    network = "default"
  }
}
